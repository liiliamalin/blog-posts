### Build the project
./gradlew clean build

### Run the application
The application runs on port 8080

./gradlew bootRun

### Run unit tests
./gradlew test



