package com.example.assessment.posts;

import com.example.assessment.posts.model.Post;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PostsSortingServiceTest {

  private static final int[] EXPECTED_ASC_ORDER = {1, 2, 3, 4};
  private static final int[] EXPECTED_DESC_ORDER = {4, 3, 2, 1};

  private final PostsSortingService postsSortingService = new PostsSortingService();

  @ParameterizedTest
  @DisplayName("Given sort by field, sort direction and unsorted posts " +
      "when sorting posts " +
      "then should return the expected sorted posts")
  @MethodSource("testData")
  void sort(String givenSortByField, String givenSortDirection, int[] expectedOrder) {
    // given
    Set<Post> givenUnsortedPosts = createPosts(3, 2, 4, 1);

    // when
    List<Post> actualSortedPosts = postsSortingService.sort(givenSortByField, givenSortDirection, givenUnsortedPosts);

    // then
    assertThat(actualSortedPosts).containsExactlyElementsOf(createPosts(expectedOrder));
  }

  @SuppressWarnings("unused") // Used via @MethodSource
  private static Stream<Arguments> testData() {
    return Stream.of(
        Arguments.of("id", "asc", EXPECTED_ASC_ORDER),
        Arguments.of("id", "desc", EXPECTED_DESC_ORDER),
        Arguments.of("reads", "asc", EXPECTED_ASC_ORDER),
        Arguments.of("reads", "desc", EXPECTED_DESC_ORDER),
        Arguments.of("likes", "asc", EXPECTED_ASC_ORDER),
        Arguments.of("likes", "desc", EXPECTED_DESC_ORDER),
        Arguments.of("popularity", "asc", EXPECTED_ASC_ORDER),
        Arguments.of("popularity", "desc", EXPECTED_DESC_ORDER)
    );
  }

  private static Set<Post> createPosts(int... values) {
    Set<Post> result = new LinkedHashSet<>();
    Arrays.stream(values).forEach(value -> {
      result.add(createPost(value));
    });
    return result;
  }

  private static Post createPost(int value) {
    Post post = new Post();
    post.setId(value);
    post.setReads(value);
    post.setLikes(value);
    post.setPopularity(BigDecimal.valueOf(value));
    return post;
  }

}