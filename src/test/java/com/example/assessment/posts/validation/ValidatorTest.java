package com.example.assessment.posts.validation;

import com.example.assessment.ApplicationProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ExtendWith(MockitoExtension.class)
class ValidatorTest {

  @Mock
  private ApplicationProperties properties;
  private Validator validator;

  @BeforeEach
  void setUp() {
    validator = new Validator(properties);
  }

  private final List<String> ALLOWED_SORT_FIELDS = List.of("id", "likes");
  private final List<String> ALLOWED_SORT_DIRECTIONS = List.of("asc");
  private final List<String> GIVEN_TAGS = List.of("tech", "culture");
  private static final String GIVEN_SORT_FIELD = "id";
  private static final String GIVEN_SORT_DIRECTION = "asc";

  @Test
  @DisplayName("Given tags, sort by field and sort direction " +
      "when validating query params " +
      "then should not throw an exception")
  void success() {
    // given
    givenProperties();

    // when then
    assertDoesNotThrow(() -> validator.validateQueryParams(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION));
  }

  @Test
  @DisplayName("Given no tags, sort by field and sort direction " +
      "when validating query params " +
      "then should throw an exception")
  void missingTags() {
    // given when
    Throwable throwable =
        catchThrowable(() -> validator.validateQueryParams(List.of(), GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION));

    // then
    assertThat(throwable)
        .isInstanceOf(ValidationException.class)
        .hasMessage("Tags parameter is required")
        .extracting("httpStatus")
        .isEqualTo(BAD_REQUEST);
  }

  @Test
  @DisplayName("Given tags, an invalid sort by field and sort direction " +
      "when validating query params " +
      "then should throw an exception")
  void invalidSortByField() {
    // given
    given(properties.getAllowedSortFields()).willReturn(ALLOWED_SORT_FIELDS);

    // when
    Throwable throwable =
        catchThrowable(() -> validator.validateQueryParams(GIVEN_TAGS, "invalid", GIVEN_SORT_DIRECTION));

    // then
    assertThat(throwable)
        .isInstanceOf(ValidationException.class)
        .hasMessage("sortBy parameter is invalid")
        .extracting("httpStatus")
        .isEqualTo(BAD_REQUEST);
  }

  @Test
  @DisplayName("Given tags, a sort by field and an invalid sort direction " +
      "when validating query params " +
      "then should throw an exception")
  void invalidSortDirection() {
    // given
    givenProperties();

    // when
    Throwable throwable =
        catchThrowable(() -> validator.validateQueryParams(GIVEN_TAGS, GIVEN_SORT_FIELD, "invalid"));

    // then
    assertThat(throwable)
        .isInstanceOf(ValidationException.class)
        .hasMessage("sortBy parameter is invalid")
        .extracting("httpStatus")
        .isEqualTo(BAD_REQUEST);
  }

  private void givenProperties() {
    given(properties.getAllowedSortDirections()).willReturn(ALLOWED_SORT_DIRECTIONS);
    given(properties.getAllowedSortFields()).willReturn(ALLOWED_SORT_FIELDS);
  }

}