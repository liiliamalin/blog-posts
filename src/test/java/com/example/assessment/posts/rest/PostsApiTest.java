package com.example.assessment.posts.rest;

import com.example.assessment.posts.PostsRetrievalService;
import com.example.assessment.posts.model.Post;
import com.example.assessment.posts.model.Posts;
import com.example.assessment.posts.validation.ValidationException;
import com.example.assessment.posts.validation.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.springframework.http.HttpStatus.*;

@ExtendWith(MockitoExtension.class)
class PostsApiTest {

  private static final List<String> GIVEN_TAGS = List.of("tech");
  private static final String GIVEN_SORT_FIELD = "id";
  private static final String GIVEN_SORT_DIRECTION = "asc";
  public static final String EXCEPTION_MESSAGE = "expected exception";

  @Mock private Validator validator;
  @Mock private PostsRetrievalService postsRetrievalService;

  private PostsApi postsApi;

  @BeforeEach
  void setUp() {
    postsApi = new PostsApi(validator, postsRetrievalService);
  }

  @Test
  @DisplayName("Given the query params tags, sort by field and sort direction " +
      "when requesting posts " +
      "then should return the expected posts and a status code 200 OK")
  void success() {
    // given
    Posts givenPosts = givenPosts();
    given(postsRetrievalService.fetchSortedBlogPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION))
        .willReturn(givenPosts);

    // when
    ResponseEntity<Object> response = postsApi.getPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // then
    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(response.getBody()).isEqualTo(givenPosts);
  }

  @Test
  @DisplayName("Given the query param validation fails " +
      "when requesting posts " +
      "then should return an error message and a status code 400 BAD REQUEST")
  void queryParamValidationFails() {
    // given
    willThrow(new ValidationException(EXCEPTION_MESSAGE, BAD_REQUEST))
        .given(validator).validateQueryParams(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // when
    ResponseEntity<Object> response = postsApi.getPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(Map.of("error", EXCEPTION_MESSAGE));
  }

  @Test
  @DisplayName("Given no posts are found for given query params " +
      "when requesting posts " +
      "then should return an error message and a status code 404 NOT FOUND")
  void noPosts() {
    // given
    Posts givenNoPosts = givenNoPosts();
    given(postsRetrievalService.fetchSortedBlogPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION))
        .willReturn(givenNoPosts);

    // when
    ResponseEntity<Object> response = postsApi.getPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(Map.of("error", "Blog posts not found"));
  }

  @Test
  @DisplayName("Given an unexpected exception is thrown " +
      "when requesting posts " +
      "then should return an error message and a status code 400 BAD REQUEST")
  void unexpectedException() {
    // given
    willThrow(new RuntimeException(EXCEPTION_MESSAGE))
        .given(validator).validateQueryParams(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // when
    ResponseEntity<Object> response = postsApi.getPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(Map.of("error", EXCEPTION_MESSAGE));
  }

  private Posts givenPosts() {
    Posts result = new Posts();
    Post post = new Post();
    post.setId(1);
    result.setPosts(List.of(post));
    return result;
  }

  private Posts givenNoPosts() {
    Posts result = new Posts();
    result.setPosts(List.of());
    return result;
  }

}