package com.example.assessment.posts;

import com.example.assessment.ApplicationProperties;
import com.example.assessment.posts.model.Post;
import com.example.assessment.posts.model.Posts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class PostsRetrievalServiceTest {

  private static final String BASE_URL = "https://test.url";
  private static final String URI = "https://test.url?tag=tech";
  private static final List<String> GIVEN_TAGS = List.of("tech");
  private static final String GIVEN_SORT_FIELD = "id";
  private static final String GIVEN_SORT_DIRECTION = "asc";

  @Mock private ApplicationProperties properties;
  @Mock private PostsSortingService postsSortingService;
  @Mock private RestTemplate restTemplate;

  private PostsRetrievalService postsRetrievalService;

  @BeforeEach
  void setUp() {
    postsRetrievalService = new PostsRetrievalService(properties, postsSortingService);
    postsRetrievalService.restTemplate = restTemplate;
    postsRetrievalService.asyncExecutor = givenExecutor();
    givenProperties();
  }

  @Test
  @DisplayName("Given tags, sort by field, and sort direction " +
      "when fetching blog posts " +
      "then should return the expected blog posts")
  void success() {
    // given
    Posts givenPosts = givenPosts(1, 2);
    given(restTemplate.getForObject(URI, Posts.class)).willReturn(givenPosts);

    // when
    postsRetrievalService.fetchSortedBlogPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // then
    then(postsSortingService).should().sort(GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION, expectedPosts(givenPosts.getPosts()));
  }

  @Test
  @DisplayName("Given no blog posts are found for the given tags " +
      "when fetching blog posts " +
      "then should not throw an error and should return an empty set")
  void postsNotFound() {
    // given
    Posts givenPosts = null;
    given(restTemplate.getForObject(URI, Posts.class)).willReturn(givenPosts);

    // when
    postsRetrievalService.fetchSortedBlogPosts(GIVEN_TAGS, GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION);

    // then
    then(postsSortingService).should().sort(GIVEN_SORT_FIELD, GIVEN_SORT_DIRECTION, Set.of());
  }

  private void givenProperties() {
    given(properties.getBaseUrl()).willReturn(BASE_URL);
  }


  private Posts givenPosts(int... values) {
    Posts result = new Posts();
    List<Post> posts = new ArrayList<>();
    Arrays.stream(values).forEach(value -> {
      posts.add(givenPost(value));
    });
    result.setPosts(posts);
    return result;
  }

  private Post givenPost(int value) {
    Post post = new Post();
    post.setId(value);
    return post;
  }

  private Set<Post> expectedPosts(List<Post> posts) {
    return new HashSet<>(posts);
  }

  private ThreadPoolTaskExecutor givenExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(2);
    executor.setMaxPoolSize(2);
    executor.initialize();
    return executor;
  }

}