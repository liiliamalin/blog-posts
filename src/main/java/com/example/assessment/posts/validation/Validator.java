package com.example.assessment.posts.validation;

import com.example.assessment.ApplicationProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class Validator {

  private final ApplicationProperties properties;

  public void validateQueryParams(List<String> tags, String sortByField, String sortDirection) {
    if (tags.isEmpty()) {
      throw new ValidationException("Tags parameter is required", HttpStatus.BAD_REQUEST);
    }

    if (!properties.getAllowedSortFields().contains(sortByField) ||
        !properties.getAllowedSortDirections().contains(sortDirection)
    ) {
      throw new ValidationException("sortBy parameter is invalid", HttpStatus.BAD_REQUEST);
    }
  }

}
