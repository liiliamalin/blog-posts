package com.example.assessment.posts.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Post {

  @EqualsAndHashCode.Include private Integer id;
  private String author;
  private Integer authorId;
  private Integer likes;
  private BigDecimal popularity;
  private Integer reads;
  private String[] tags;

}
