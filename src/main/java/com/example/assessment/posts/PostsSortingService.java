package com.example.assessment.posts;

import com.example.assessment.posts.model.Post;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@Service
public class PostsSortingService {

  private  final Comparator<Post> compareById = Comparator.comparing(Post::getId);
  private  final Comparator<Post> compareByReads = Comparator.comparing(Post::getReads);
  private  final Comparator<Post> compareByLikes = Comparator.comparing(Post::getLikes);
  private  final Comparator<Post> compareByPopularity = Comparator.comparing(Post::getPopularity);

  public List<Post> sort(String sortByField, String sortDirection, Set<Post> unsortedPosts) {
    List<Post> posts = new ArrayList<>(unsortedPosts);
    if ("asc".equals(sortDirection)) {
      if ("id".equals(sortByField)) {
        posts.sort(compareById);
      }
      if ("reads".equals(sortByField)) {
        posts.sort(compareByReads);
      }
      if ("likes".equals(sortByField)) {
        posts.sort(compareByLikes);
      }
      if ("popularity".equals(sortByField)) {
        posts.sort(compareByPopularity);
      }
    }
    if ("desc".equals(sortDirection)) {
      if ("id".equals(sortByField)) {
        posts.sort(compareById.reversed());
      }
      if ("reads".equals(sortByField)) {
        posts.sort(compareByReads.reversed());
      }
      if ("likes".equals(sortByField)) {
        posts.sort(compareByLikes.reversed());
      }
      if ("popularity".equals(sortByField)) {
        posts.sort(compareByPopularity.reversed());
      }
    }
    return posts;
  }

}
