package com.example.assessment.posts;

import com.example.assessment.ApplicationProperties;
import com.example.assessment.posts.model.Post;
import com.example.assessment.posts.model.Posts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Service
@EnableCaching
@RequiredArgsConstructor
public class PostsRetrievalService {

  private final ApplicationProperties properties;
  private final PostsSortingService postsSortingService;

  @Autowired RestTemplate restTemplate;
  @Autowired Executor asyncExecutor;

  @Cacheable(value = "posts")
  public Posts fetchSortedBlogPosts(List<String> tags, String sortByField, String sortDirection) {
    List<CompletableFuture<List<Post>>> allFutures = tags.stream()
        .map(this::fetchPosts)
        .collect(Collectors.toList());

    Set<Post> unsortedPosts = allFutures.stream()
        .map(completableFuture -> {
          try {
            return completableFuture.join();
          } catch (Exception e) {
            return null;
          }
        })
        .filter(Objects::nonNull)
        .flatMap(Collection::stream)
        .collect(Collectors.toSet());

    Posts posts = new Posts();
    posts.setPosts(postsSortingService.sort(sortByField, sortDirection, unsortedPosts));
    return posts;
  }

  private CompletableFuture<List<Post>> fetchPosts(String tag) {
    String uri = UriComponentsBuilder.fromHttpUrl(properties.getBaseUrl()).queryParam("tag", tag).toUriString();
    return CompletableFuture.supplyAsync(() -> restTemplate.getForObject(uri, Posts.class), asyncExecutor)
        .thenApply(Posts::getPosts);
  }

}


