package com.example.assessment.posts;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CacheEvictionScheduler {

  @Scheduled(cron = "${posts.cache-eviction.cron-pattern}")
  @CacheEvict(allEntries = true, value = "posts")
  public void evictCache() {
    System.out.println("Flushing cache");
  }

}


