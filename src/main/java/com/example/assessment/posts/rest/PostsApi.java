package com.example.assessment.posts.rest;

import com.example.assessment.posts.PostsRetrievalService;
import com.example.assessment.posts.model.Posts;
import com.example.assessment.posts.validation.ValidationException;
import com.example.assessment.posts.validation.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class PostsApi {

  private final Validator validator;
  private final PostsRetrievalService postsRetrievalService;

  @ResponseBody
  @GetMapping("/posts")
  public ResponseEntity<Object> getPosts(
      @RequestParam(name = "tags") List<String> tags,
      @RequestParam(name = "sortBy", required = false, defaultValue = "id") String sortByField,
      @RequestParam(name = "direction", required = false, defaultValue = "asc") String sortDirection
  ) {
    try {
      validator.validateQueryParams(tags, sortByField, sortDirection);
      Posts posts = postsRetrievalService.fetchSortedBlogPosts(tags, sortByField, sortDirection);
      if (posts.getPosts().isEmpty()) {
        return errorResponse("Blog posts not found", HttpStatus.NOT_FOUND);
      }
      return ResponseEntity.ok(posts);
    } catch (ValidationException e) {
      return errorResponse(e.getMessage(), e.getHttpStatus());
    } catch (Exception e) {
      return errorResponse(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  private ResponseEntity<Object> errorResponse(String message, HttpStatus statusCode) {
    return new ResponseEntity<>(Map.of("error", message), statusCode);
  }

}

