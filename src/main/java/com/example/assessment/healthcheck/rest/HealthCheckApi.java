package com.example.assessment.healthcheck.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
public class HealthCheckApi {

  @ResponseBody
  @GetMapping(value = "/ping", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> checkHealth() {
    return new ResponseEntity<>(Map.of("success", true), HttpStatus.OK);
  }

}
