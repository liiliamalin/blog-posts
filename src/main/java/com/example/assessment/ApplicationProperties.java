package com.example.assessment;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "posts")
public class ApplicationProperties {

    private String baseUrl;
    private List<String> allowedSortFields;
    private List<String> allowedSortDirections;

}

